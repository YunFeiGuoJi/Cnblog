net,ipv4.ip_local_port_range:
  sysctl.present:
    - value: 1000 6500
fs.file-max:
  sysctl.present:
    -value: 2000000
net.ipv4_ip_forward:
  sysctl.present:
    -value: 1
vm.swappiness:
  sysctl.present:
    -value: 0