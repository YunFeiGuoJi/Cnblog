/etc/resolv.conf:
  file.manged:
    - source: salt://init/files/resolv.conf
    - user: root
    - group: root
    - mode: 644