#!/usr/bin/env python3

import requests
import json
import re


def main():
    gitlabAddr = "gitlab.dadiyw.com"
    gitlabToken = "WWsCKT2x3eaMvpKfizgM"
    url = "https://%s/api/v4/projects?private_token=%s&per_page=100&page=%d&order_by=name" % (
        gitlabAddr, gitlabToken, 1)
    r = requests.get(url)
    dp = json.loads(r.text)
    #print(json.dumps(dp[-1], indent=4,sort_keys=True))
    git_list = []
    for i in dp:
        git_repo_logpath = json.dumps(
            i['http_url_to_repo'], sort_keys=True, indent=4)
        git_repo = re.findall(r'https://(.+?.*)"', git_repo_logpath)
        git_path_sortpath = re.match(r'(")(\w+.*)(.git)', git_repo_logpath)
        git_path = git_path_sortpath.group(2).split('/', 5)
        git_repo_dist = {
            'git_repo': git_repo[0],
            'git_path': [git_path[-1],git_path[-2]]
        }
        git_list.append(git_repo_dist)
    return git_list


if __name__ == '__main__':
    git_repo = main()
    print(git_repo)
