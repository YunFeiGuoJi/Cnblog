#!/usr/bin bash

ansible-playbook --inventory-file='./../inventory/inventory.ini' ./deploy.yml -e pwd='${var.pwd}' -e target='pankuibo' --tags='docker_compose' --forks=5 --user='root'