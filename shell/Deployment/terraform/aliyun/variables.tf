variable "region" {
  default = "cn-hongkong"
}

variable "minikube_host_ip" {
  default = "172.31.49.221"
}

variable "pwd" {
  type    = string
  default = ""
}