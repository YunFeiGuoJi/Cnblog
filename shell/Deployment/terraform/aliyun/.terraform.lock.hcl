# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/alicloud" {
  version = "1.156.0"
  hashes = [
    "h1:43NDtUBnF+UEv6REqENhIskbTLMntdxDtgrbfqLytj4=",
    "h1:nq9CmL2jnQWw/5zGeFTSbhcc6pWFnUF9Wk4/uVbIIOE=",
    "zh:13e76441913807e7a0500cd007603740ff44ba5991831c735c9713e35907bce9",
    "zh:2658d9a4f1f43ccb17dfb2ba43923bb8f2fc0c1e10a84c86a94a1ea9d74f3c30",
    "zh:42ea19a635144ee68f91935ed08ae160f774659d23185c8d869b1fcf7cffb4de",
    "zh:4adad8e0962f4f518ecced5094df94b5e980a50e195fe09fbe769f288ac96afc",
    "zh:5ccd6f013cc9cf8d7d585f37d38fc98216b5c43bc220a528611f39ad8971c044",
    "zh:6c2ca3f9230e83a83f15d18bf6c620e7efd2ef192ffc669fc684eec9716ed942",
    "zh:6e57cad8ea3c578b7eee47eb08a29f798b0351e723288977e56c076dbbcba17c",
    "zh:89a56df0640dab802279123f50dc9265d05063897aa70a26418968659553a860",
    "zh:9fb5ac11ca0a55c300250f370bb48aa53ab21e3fd00c37df99087964d1955a97",
    "zh:bba43f821b6cb7e5b6cacc898c6ee791432c7a98026093ec01c7a464ebbaffa9",
    "zh:e5245523467f4a4acaf8239c7ff19257cb527b2537eeec865ed1f99cf99144e4",
    "zh:f780488d1ac26926654efadcfb7fa5ee1b1f5a4ed9f42a7c3611962b017e230b",
    "zh:fac04a7940276d45e7fe097bf29f38077485f3a8d745a37545e20daa9c173439",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.0"
  hashes = [
    "h1:vpC6bgUQoJ0znqIKVFevOdq+YQw42bRq0u+H3nto8nA=",
    "h1:xhbHC6in3nQryvTQBWKxebi3inG5OCgHgc4fRxL0ymc=",
    "zh:02a1675fd8de126a00460942aaae242e65ca3380b5bb192e8773ef3da9073fd2",
    "zh:53e30545ff8926a8e30ad30648991ca8b93b6fa496272cd23b26763c8ee84515",
    "zh:5f9200bf708913621d0f6514179d89700e9aa3097c77dac730e8ba6e5901d521",
    "zh:9ebf4d9704faba06b3ec7242c773c0fbfe12d62db7d00356d4f55385fc69bfb2",
    "zh:a6576c81adc70326e4e1c999c04ad9ca37113a6e925aefab4765e5a5198efa7e",
    "zh:a8a42d13346347aff6c63a37cda9b2c6aa5cc384a55b2fe6d6adfa390e609c53",
    "zh:c797744d08a5307d50210e0454f91ca4d1c7621c68740441cf4579390452321d",
    "zh:cecb6a304046df34c11229f20a80b24b1603960b794d68361a67c5efe58e62b8",
    "zh:e1371aa1e502000d9974cfaff5be4cfa02f47b17400005a16f14d2ef30dc2a70",
    "zh:fc39cc1fe71234a0b0369d5c5c7f876c71b956d23d7d6f518289737a001ba69b",
    "zh:fea4227271ebf7d9e2b61b89ce2328c7262acd9fd190e1fd6d15a591abfa848e",
  ]
}
