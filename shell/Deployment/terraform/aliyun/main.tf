provider "alicloud" {
}

data "alicloud_instances" "poc" {
    status = "Running"
}

data "alicloud_instances" "dev" {
    name_regex  = "^dev"
    status = "Running"
}

data "alicloud_instances" "test" {
    name_regex  = "^test"
    status = "Running"
}

data "alicloud_instances" "uat" {
    name_regex  = "^uat"
    status = "Running"
}

data "alicloud_instances" "prod" {
    name_regex  = "^prod"
    status = "Running"
}


locals {
    instance_id = element(data.alicloud_instances.poc.*,0)
}

resource "null_resource" "CreateAnsibleInventoryFile" {
    provisioner "local-exec" {
        command = "[[ -f ${path.root}/../ansible/inventory/inventory.ini ]] || touch ${path.root}/../ansible/inventory/inventory.ini"
    }

    connection {
        type                = "ssh"
        user                = "root"
        private_key         = file("~/.ssh/id_rsa")
        #host                = join(",",element(data.alicloud_instances.poc.*.instances[*][*].private_ip,0))
        host                = "172.31.49.221"
    } 
}

resource "local_file" "AnsibleInventory" {
    content                 = templatefile("${path.root}/../ansible/inventory/inventory.tftpl",{
        vm_ip_dev           = element(data.alicloud_instances.dev.*.instances[*][*].private_ip,0),
        vm_ip_test          = element(data.alicloud_instances.test.*.instances[*][*].private_ip,0),
        vm_ip_uat           = element(data.alicloud_instances.uat.*.instances[*][*].private_ip,0),
        vm_ip_prod          = element(data.alicloud_instances.prod.*.instances[*][*].private_ip,0)
        vm_ip_hk            = element(data.alicloud_instances.poc.*.instances[*][*].private_ip,0) 
    })
    filename                = "${path.root}/../ansible/inventory/inventory.ini"
    directory_permission    = "0755"
    file_permission         = "0755"
}

resource "null_resource" "AnsibleInventory" {
    depends_on = [null_resource.CreateAnsibleInventoryFile,local_file.AnsibleInventory]
    provisioner "local-exec" {
        command = "sed -i '/^[  ]*$/d' ${path.root}/../ansible/inventory/inventory.ini"
    }

    connection {
        type                = "ssh"
        user                = "root"
        private_key         = file("~/.ssh/id_rsa")
        #host                = join(",",element(data.alicloud_instances.poc.*.instances[*][*].private_ip,0))
        host                = "172.31.49.221"
    }
}

resource "null_resource" "poc" {
    depends_on = [data.alicloud_instances.poc,null_resource.AnsibleInventory,null_resource.CreateAnsibleInventoryFile]

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-playbook --inventory-file='./../ansible/inventory/inventory.ini' ./../ansible/playbooks/deploy.yml -e pwd='${var.pwd}' --tags='common,container' --forks=5 --user='root'"
    }

    connection {
        type                = "ssh"
        user                = "root"
        #private_key         = file("~/.ssh/id_rsa")
        host_key            = file("~/.ssh/id_rsa") 
        #host                = join(",",element(data.alicloud_instances.poc.*.instances[*][*].private_ip,0))
        host                = "172.31.49.221"
    }
}


module "dns" {
 source = "./modules/dns"
 records = [
   {
     rr       = "nacos-hk"
     type     = "A"
     ttl      = 600
     value    = "47.243.34.121"
   },
   {
     rr       = "gateay-dev"
     type     = "A"
     ttl      = 600
     value    = "123.57.241.90" 
   }
 ]
}

# module "nat_gateways" {
#     source = "./modules/nat"
#     forward_entrys = [
#       {
#           internal_ip      = "10.101.0.46"
#           internal_port    = "22"
#           forward_entry_name = "lvm_test"
#           external_port    = "12030"
#       },
#       {
#           internal_ip      = "10.101.16.1"
#           internal_port    = "8080"
#           forward_entry_name = "fotbar_test"
#           external_port    = "8080" 
#       },
#       {
#           internal_ip      = "10.101.17.0"
#           internal_port    = "22"
#           forward_entry_name = "test-scrm-k8s-node02-ssh"
#           external_port    = "12011" 
#       }, 
#   ]
#   snat_entrys = [
#       {
#           snat_entry_name  = ""
#       }
#   ]
# }

# module "ack_cs_managed" {
#   source = "./modules/ack"
# }

# # provider "kubernetes" {
# #     host                   = "https://${var.minikube_host_ip}:8443"
# #     client_certificate     = file("~/.minikube/profiles/minikube/client.crt")
# #     client_key             = file("~/.minikube/profiles/minikube/client.key")
# #     cluster_ca_certificate = file("~/.minikube/ca.crt")
# # }

# module "kubernetes_manifest" {
#   source = "./modules/kubernetes"
#   pods = [
#       {
#         pod_name          = "label-demo01"
#         ns                = "default"
#         container    = [
#             {
#                 container_name  = "nginx-demo01"
#                 image           = "busybox"
#                 container_port  = "80"
#             },
#             {
#                 container_name  = "nginx-demo02"
#                 image           = "busybox"
#                 container_port  = "80"
#             }
#         ]
#       },
#       {
#         pod_name          = "label-demo02"
#         ns                = "default"
#         container    = [
#             {
#                 container_name  = "nginx-demo03"
#                 image           = "nginx"
#                 container_port  = "80"
#             },
#             {
#                 container_name  = "nginx-demo04"
#                 image           = "nginx"
#                 container_port  = "80"
#             }
#         ]
#       },
#       {
#         pod_name          = "label-demo03"
#         ns                = "default"
#         container    = [
#             {
#                 container_name  = "nginx-demo05"
#                 image           = "nginx"
#                 container_port  = "8081"
#             },
#             {
#                 container_name  = "nginx-demo06"
#                 image           = "tomaszguzialek/flask-api"
#                 container_port  = "5000"
#             }
#         ]
#       },
#   ]
# }

