output "pods" {
  value = data.kubernetes_pod.pod.spec
}

output "deploy" {
  value = kubernetes_deployment.deploy
}
