variable "pods" {
    type        = any
    default     = []
}

variable "pod_list" {
    type        = any
    default     = []
}

variable "add_pods" {
    type        = bool
    default     = true
}