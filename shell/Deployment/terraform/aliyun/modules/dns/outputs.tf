output "records" {
  value = data.alicloud_dns_records.record
}

output "record" {
  value = alicloud_dns_record.record
}