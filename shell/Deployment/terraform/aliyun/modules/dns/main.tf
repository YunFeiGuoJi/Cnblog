data "alicloud_dns_domains" "domain" {
  domain_name_regex = "^*."
}

data "alicloud_dns_groups" "group" {
  name_regex = "^y[A-Za-z]+"
}

data "alicloud_dns_records" "record" {
  count = length(data.alicloud_dns_domains.domain.domains)
  domain_name       = data.alicloud_dns_domains.domain.domains[count.index].domain_name
  is_locked         = false
  type              = "A"
  output_file       = "records.txt"
}

locals {
    records     = length(var.records) > 0 ? var.records: var.record_list
}

resource "alicloud_dns_record" "record" {
    count = var.add_records ? length(local.records) : 0
    name            = var.domain_name
    host_record     = lookup(local.records[count.index],"rr","") !="" ? lookup(local.records[count.index],"rr"): lookup(local.records[count.index],"name")
    type            = "A"
    value           = lookup(local.records[count.index],"value")
}



