variable "number" {
  default = "1"
}

variable "domain_name" {
  default = "01member.com"
}

variable "records" {
    type        = list(map(string))
    default     = []
}

variable "record_list" {
    type        = list(map(string))
    default     = []
}

variable "add_records" {
  description = "Whether to add records to Private Zone."
  type        = bool
  default     = true
}