data "alicloud_cs_managed_kubernetes_clusters" "k8s_clusters" {
  name_regex  = "ddyw-member-test"
  output_file = "my-first-k8s-json"
  enable_details = true
}