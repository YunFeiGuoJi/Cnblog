variable "forward_entrys" {
    type        = any
    default     = []
}

variable "snat_entrys" {
    type        = any
    default     = []
}