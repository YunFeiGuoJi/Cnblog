output "nat_gateways" {
    value = data.alicloud_nat_gateways.nat_gateways
}

output "route_tables" {
    value = data.alicloud_route_tables.route_tables
}

output "route_entries" {
    value = data.alicloud_route_entries.route_entries
}

output "network_acls" {
    value = data.alicloud_network_acls.network_acls
}

output "forward_entries" {
    value = data.alicloud_forward_entries.forward_entries
}

output "eip_ds" {
    value = data.alicloud_eips.eips_ds
}

output "snat_eips_addr" {
    value = data.alicloud_eip_addresses.snat_eips_addr.eips[0].ip_address
}

output "dnat_eips_addr" {
    value = data.alicloud_eip_addresses.dnat_eips_addr.eips[0].ip_address
}

