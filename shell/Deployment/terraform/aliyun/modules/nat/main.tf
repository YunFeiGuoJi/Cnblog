data "alicloud_zones" "zones" {
    available_resource_creation = "VSwitch"
}

data "alicloud_nat_gateways" "nat_gateways" {
    name_regex = "^test"
}

data "alicloud_route_tables" "route_tables" {
}

data "alicloud_route_entries" "route_entries" {
    route_table_id = data.alicloud_route_tables.route_tables.ids[0]
}

data "alicloud_network_acls" "network_acls" {
}

data "alicloud_eips" "eips_ds" { 
}

data "alicloud_eip_addresses" "snat_eips_addr" {
   name_regex = "test-scrm-k8s-snat" 
}

data "alicloud_eip_addresses" "dnat_eips_addr" {
   name_regex = "test-scrm-k8s-dnat" 
}

data "alicloud_forward_entries" "forward_entries" {
    forward_table_id = data.alicloud_nat_gateways.nat_gateways.gateways[0].forward_table_ids[0]
}

resource "alicloud_forward_entry" "forward_entry" {
    count = length(var.forward_entrys)
    forward_table_id = data.alicloud_nat_gateways.nat_gateways.gateways[0].forward_table_ids[0]
    forward_entry_name = lookup(var.forward_entrys[count.index],"forward_entry_name")
    #external_ip = data.alicloud_eip_addresses.dnat_eips_addr.eips[0].ip_address
    external_ip = data.alicloud_eip_addresses.dnat_eips_addr.*.ip_address
    external_port    = lookup(var.forward_entrys[count.index],"external_port")
    ip_protocol      = "tcp"
    internal_ip      = lookup(var.forward_entrys[count.index],"internal_ip") 
    internal_port    = lookup(var.forward_entrys[count.index],"internal_port")
}

resource "alicloud_snat_entry" "snat_entry" {
    count = length(var.snat_entrys)
    snat_table_id = data.alicloud_nat_gateways.nat_gateways.gateways[0].snat_tables_ids[0]
    snat_ip = data.alicloud_eip_addresses.snat_eips_addr.*.ip_address
    snat_entry_name = lookup(var.snat_entrys[count.index],"snat_entry_name")
}
