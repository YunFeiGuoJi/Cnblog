output "hostname_list" {
    value = element(data.alicloud_instances.poc.*.instances[*][*].name,0)
}

output "ecs_ids" {
    value = element(data.alicloud_instances.poc.*.ids,0)
}

output "ecs_private_ip" {
    value = element(data.alicloud_instances.poc.*.instances[*][*].private_ip,0)
}

output "tags" {
    value = element(data.alicloud_instances.poc.*.instances[*][*].tags,0)
}

output "tags-1" {
    value = element(data.alicloud_instances.poc.*.instances[*][*].status,0)
}

# output "nat_gateways" {
#     value = module.nat_gateways.nat_gateways
# }

# output "route_tables" {
#     value = module.nat_gateways.route_tables
# }

# output "route_entries" {
#     value = module.nat_gateways.route_entries
# }

# output "network_acls" {
#     value = module.nat_gateways.network_acls
# }

# output "forward_entries" {
#     value = module.nat_gateways.forward_entries
# }

# output "eips_ds" {
#     value = module.nat_gateways.eip_ds
# }

# output "snat_eips_addr" {
#     value = module.nat_gateways.snat_eips_addr
# }

# output "dnat_eips_addr" {
#     value = module.nat_gateways.dnat_eips_addr
# }


#output "domain_record" {
#    value = module.dns.record
#}
#
#output "domain_raws" {
#    value = length(module.dns.records.urls)
#}

# output "kubernetes_cluster" {
#     value = module.ack_cs_managed.clusters
  
# }

# output "pods" {
#   value = module.kubernetes_manifest.pods
# }

# output "deploy" {
#   value = module.kubernetes_manifest.deploy
# }
