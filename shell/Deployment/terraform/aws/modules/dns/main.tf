//导入资源
//terraform import aws_service_discovery_service.example 0123456789

resource "aws_service_discovery_service" "example" {
  name = "example"

  dns_config {
    namespace_id = aws_service_discovery_public_dns_namespace.example.id

    dns_records {
      ttl  = 10
      type = "A"
    }
  }

  health_check_config {
    failure_threshold = 10
    resource_path     = "path"
    type              = "HTTP"
  }
}


resource "aws_service_discovery_instance" "example" {
  instance_id = "example-instance-id"
  service_id  = aws_service_discovery_service.example.id

  attributes = {
    AWS_INSTANCE_IPV4 = "172.18.0.1"
    custom_attribute  = "custom"
  }
}