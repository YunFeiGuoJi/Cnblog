/* 
配置secret
export AWS_ACCESS_KEY_ID="anaccesskey"
export AWS_SECRET_ACCESS_KEY="asecretkey"
export AWS_REGION="us-west-2"
terraform plan
*/


provider "aws" {
}

data "aws_instances" "poc" {
    instance_status_names = ["Running"]
}

data "aws_instances" "dev" {
    name_regex  = "^dev"
    instance_status_names = ["Running"]
}

data "aws_instances" "test" {
    name_regex  = "^test"
    instance_status_names = ["Running"]
}

data "aws_instances" "uat" {
    name_regex  = "^uat"
    instance_status_names = ["Running"]
}

data "aws_instances" "prod" {
    name_regex  = "^prod"
    instance_status_names = ["Running"]
}


locals {
    instance_id = element(data.aws_instances.poc.*,0)
}

resource "null_resource" "CreateAnsibleInventoryFile" {
    provisioner "local-exec" {
        command = "[[ -f ${path.root}/../ansible/inventory/inventory.ini ]] || touch ${path.root}/../ansible/inventory/inventory.ini"
    }

    connection {
        type                = "ssh"
        user                = "root"
        private_key         = file("~/.ssh/id_rsa")
        #host                = join(",",element(data.aws_instances.poc.*.instances[*][*].private_ip,0))
        host                = "${var.host}"
    } 
}

resource "local_file" "AnsibleInventory" {
    content                 = templatefile("${path.root}/../ansible/inventory/inventory.tftpl",{
        vm_ip_dev           = element(data.aws_instances.dev.*.instances[*][*].private_ip,0),
        vm_ip_test          = element(data.aws_instances.test.*.instances[*][*].private_ip,0),
        vm_ip_uat           = element(data.aws_instances.uat.*.instances[*][*].private_ip,0),
        vm_ip_prod          = element(data.aws_instances.prod.*.instances[*][*].private_ip,0)
        vm_ip_hk            = element(data.aws_instances.poc.*.instances[*][*].private_ip,0) 
    })
    filename                = "${path.root}/../ansible/inventory/inventory.ini"
    directory_permission    = "0755"
    file_permission         = "0755"
}

resource "null_resource" "AnsibleInventory" {
    depends_on = [null_resource.CreateAnsibleInventoryFile,local_file.AnsibleInventory]
    provisioner "local-exec" {
        command = "sed -i '/^[  ]*$/d' ${path.root}/../ansible/inventory/inventory.ini"
    }

    connection {
        type                = "ssh"
        user                = "root"
        private_key         = file("~/.ssh/id_rsa")
        #host                = join(",",element(data.aws_instances.poc.*.instances[*][*].private_ip,0))
        host                = "${var.host}"
    }
}

resource "null_resource" "poc" {
    depends_on = [data.aws_instances.poc,null_resource.AnsibleInventory,null_resource.CreateAnsibleInventoryFile]

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-playbook --inventory-file='./../ansible/inventory/inventory.ini' ./../ansible/playbooks/deploy.yml -e pwd='${var.pwd}' --tags='common,container' --forks=5 --user='root'"
    }

    connection {
        type                = "ssh"
        user                = "root"
        #private_key         = file("~/.ssh/id_rsa")
        host_key            = file("~/.ssh/id_rsa") 
        #host                = join(",",element(data.aws_instances.poc.*.instances[*][*].private_ip,0))
        host                = "${var.host}"
    }
}


module "dns" {
 source = "./modules/dns"
}