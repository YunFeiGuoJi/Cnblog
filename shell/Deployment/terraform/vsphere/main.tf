/* 
Document: https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs#notes-on-required-privileges
terraform plan
*/

provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "datacenter" {
}