variable "region" {
  default = "cn-hongkong"
}

variable "host" {
  default = "localhost"
}

variable "pwd" {
  type    = string
  default = ""
}