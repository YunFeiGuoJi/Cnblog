# terraform {
#   required_providers {
#     shell = {
#       source = "scottwinkler/shell"
#       version = "1.7.7"
#     }
#   }
# }

resource "null_resource" "CreateAnsibleInventoryFile" {
    provisioner "local-exec" {
        command = "[[ -f ${path.root}/../ansible/inventory/inventory.ini ]] || touch ${path.root}/../ansible/inventory/inventory.ini"
    }
}

resource "local_file" "AnsibleInventory" {
    content                 = templatefile("${path.root}/../ansible/inventory/inventory.idc.tftpl",{
        vm_ip_test          = var.idc_instances
    })
    filename                = "${path.root}/../ansible/inventory/inventory.ini"
    directory_permission    = "0755"
    file_permission         = "0755"
}

resource "null_resource" "AnsibleInventory" {
    depends_on = [null_resource.CreateAnsibleInventoryFile,local_file.AnsibleInventory]
    provisioner "local-exec" {
        command = "sed -i '/^[  ]*$/d' ${path.root}/../ansible/inventory/inventory.ini"
    }
}

resource "null_resource" "poc" {

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-playbook --inventory-file='./../ansible/inventory/inventory.ini' ./../ansible/playbooks/deploy.yml -e pwd='${var.pwd}' -e target='${var.target}' --tags='common,container' --forks=5 --user='root'"
    }
}

