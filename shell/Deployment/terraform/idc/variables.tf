variable "idc_instances" {
  default = [
    "localhost",
  ]
}

variable "pwd" {
  default = ""
}

variable "target" {
  default = ""
}