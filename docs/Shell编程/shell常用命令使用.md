### 文件属性
chattr 命令用于改变文件属性
这项指令可改变存放在ext2文件系统上的文件或目录属性，这些属性共有以下8种模式：

- a：让文件或目录仅供附加用途。
- b：不更新文件或目录的最后存取时间。
- c：将文件或目录压缩后存放。
- d：将文件或目录排除在倾倒操作之外。
- i：不得任意更动文件或目录。
- s：保密性删除文件或目录。
- S：即时更新文件或目录。
- u：预防意外删除。

**语法**
```bash
chattr [-RV][-v<版本编号>][+/-/=<属性>][文件或目录...]
```

**参数**
- -R 递归处理，将指定目录下的所有文件及子目录一并处理。
- -v<版本编号> 设置文件或目录版本。
- -V 显示指令执行过程。
　　+<属性> 开启文件或目录的该项属性。
　　-<属性> 关闭文件或目录的该项属性。
　　=<属性> 指定文件或目录的该项属性。

**实例**
```bash
# 不能改动文件
chattr +i /etc/resolv.conf
# 查看权限
lsattr /etc/resolv.conf
----i-------- /etc/resolv.conf
# 只能追加不能删除
chattr +a /var/log/messages
```

### jq命令使用

**实例**

有一json文件(test.json)内容如下，使用jq做相关数据提出
```json
{
    "apiVersion": "v1",
    "kind": "Service",
    "metadata": {
        "annotations": {
            "meta.helm.sh/release-name": "prometheus",
            "meta.helm.sh/release-namespace": "kube-system"
        },
        "creationTimestamp": "2022-05-05T09:19:16Z",
        "labels": {
            "app": "prometheus",
            "app.kubernetes.io/managed-by": "Helm",
            "chart": "prometheus-11.12.1",
            "component": "server",
            "heritage": "Helm",
            "release": "prometheus"
        },
        "managedFields": [
            {
                "apiVersion": "v1",
                "fieldsType": "FieldsV1",
                "fieldsV1": {
                    "f:metadata": {
                        "f:annotations": {
                            ".": {},
                            "f:meta.helm.sh/release-name": {},
                            "f:meta.helm.sh/release-namespace": {}
                        },
                        "f:labels": {
                            ".": {},
                            "f:app": {},
                            "f:app.kubernetes.io/managed-by": {},
                            "f:chart": {},
                            "f:component": {},
                            "f:heritage": {},
                            "f:release": {}
                        }
                    },
                    "f:spec": {
                        "f:externalTrafficPolicy": {},
                        "f:ports": {
                            ".": {},
                            "k:{\"port\":80,\"protocol\":\"TCP\"}": {
                                ".": {},
                                "f:name": {},
                                "f:nodePort": {},
                                "f:port": {},
                                "f:protocol": {},
                                "f:targetPort": {}
                            }
                        },
                        "f:selector": {
                            ".": {},
                            "f:app": {},
                            "f:component": {},
                            "f:release": {}
                        },
                        "f:sessionAffinity": {},
                        "f:type": {}
                    }
                },
                "manager": "Go-http-client",
                "operation": "Update",
                "time": "2022-05-05T09:19:16Z"
            }
        ],
        "name": "prometheus-server",
        "namespace": "kube-system",
        "resourceVersion": "54247",
        "selfLink": "/api/v1/namespaces/kube-system/services/prometheus-server",
        "uid": "8eb54da5-f64e-4ea4-a5d1-2493bc1673cd"
    },
    "spec": {
        "clusterIP": "10.96.3.118",
        "externalTrafficPolicy": "Cluster",
        "ports": [
            {
                "name": "http",
                "nodePort": 31090,
                "port": 80,
                "protocol": "TCP",
                "targetPort": 9090
            }
        ],
        "selector": {
            "app": "prometheus",
            "component": "server",
            "release": "prometheus"
        },
        "sessionAffinity": "None",
        "type": "NodePort"
    },
    "status": {
        "loadBalancer": {}
    }
}
```

```bash
# 获取一个键的值
cat test.json | jq .kind.labels
```

### awk

**实例**


### 组合命令使用
1、查出哪个IP地址连接最多
```bash
netstat -na|grep ESTABLISHED|awk '{print $5}'|awk -F: '{print $1}'|sed '/^$/d'|sort|uniq -c|sort -rn
```
2、查询nignx中那个IP访问最多
```bash

```






